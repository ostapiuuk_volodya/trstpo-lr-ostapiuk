#include<iostream>
#include <list>
#include <limits.h>
#include <chrono>
#include <math.h>
#include <vector>
using namespace std;
using Matrix = std::vector<std::vector<int>>;
int main() {
	setlocale(LC_CTYPE, "ukr");
	size_t rozmir = 20;
	size_t VisotaM = 150;
	size_t ShirinaM = 150;
	srand(14159);
	Matrix massiv;
	massiv.reserve(VisotaM);
	// Заповнення масиву
	for (int i = 0; i < VisotaM; ++i) {
		std::vector<int> v;
		v.reserve(ShirinaM);
		for (int j = 0; j < ShirinaM; ++j) {
			v.push_back(rand() % rozmir);
		}
		massiv.push_back(std::move(v));
	}
	// Обрахунок
	const double e = 2.71828;
	unsigned long long summa = 0;
#pragma omp parallel for shared(summa)   //загальна змінна
	for (int i = 0; i < VisotaM; ++i) {
		for (int j = 0; j < ShirinaM; ++j) {
			auto p = massiv[i][j];  //присвоєння змінній р значення масиву
			auto znach = 1 / (sin(pow(e, p) - 1));   //обчислення формули
			summa += znach;   //сумування значень
		}
	}
	cout << "Результат паралельного обчислення формули: 1 / (sin(pow(e, p) - 1)) = " << summa << endl;
	system("pause");
	return 0;
}