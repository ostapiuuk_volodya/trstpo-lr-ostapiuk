#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <omp.h>
#include <iostream>
using namespace std;
void zlittja(int a[], int size, int temp[]);
void zlittja_odnopotokove(int a[], int size, int temp[]);
void zlittja_paralelne(int a[], int size, int temp[], int threads);
int main(int argc, char *argv[]) {
	setlocale(LC_CTYPE, "ukr");
	int rozmir = 16;
	int potok = 4;
	int *massiv = (int*)malloc(sizeof (int)* rozmir);
	int *konteiner = (int*)malloc(sizeof (int)* rozmir);
	// Ініціалізація
	srand(14159);
	for (int i = 0; i < rozmir; i++)
		massiv[i] = rand() % rozmir;
	cout << "Випадково-створений одновимірний масив: ";
	for (int i = 0; i < rozmir; i++)
		cout << massiv[i] << ' ';
	cout << endl;
	// Сортування
	omp_set_nested(1); //включає вкладення паралелізму
	zlittja_paralelne(massiv, rozmir, konteiner, potok); //сортування злиттям з використанням рекурсії
	// Виведення
	cout << "Відсортований злиттям з використанням рекурсії масив: ";
	for (int i = 0; i < rozmir; i++)
		cout << massiv[i] << ' ';
	cout << endl;
	system("pause");
	return 0;
}

void zlittja_paralelne(int massiv[], int rozmir, int konteiner[], int potok) {
	if (potok == 1) zlittja_odnopotokove(massiv, rozmir, konteiner);
	else {
#pragma omp parallel sections
		{
#pragma omp section
			{
				zlittja_paralelne(massiv, rozmir / 2, konteiner, potok / 2);
			}
#pragma omp section
			{
			zlittja_paralelne(massiv + rozmir / 2, rozmir - rozmir / 2, konteiner + rozmir / 2, potok - potok / 2);
		}
		}
		zlittja(massiv, rozmir, konteiner);
	}
}

void zlittja_odnopotokove(int massiv[], int rozmir, int konteiner[]) {
	if (rozmir <= 2) {
		if (massiv[0] > massiv[1])
		{
			int t = massiv[1];
			massiv[1] = massiv[0];
			massiv[0] = t;
		}
		return;
	}
	zlittja_odnopotokove(massiv, rozmir / 2, konteiner);
	zlittja_odnopotokove(massiv + rozmir / 2, rozmir - rozmir / 2, konteiner);

	zlittja(massiv, rozmir, konteiner);
}

void zlittja(int massiv[], int rozmir, int konteiner[]){
	int element1 = 0;
	int element2 = rozmir / 2;
	int kont = 0;
	while (element1 < rozmir / 2 && element2 < rozmir) {
		if (massiv[element1] < massiv[element2])	{
			konteiner[kont] = massiv[element1];
			element1++;
		}
		else {
			konteiner[kont] = massiv[element2];
			element2++;
		}
		kont++;
	}
	while (element1 < rozmir / 2) {
		konteiner[kont] = massiv[element1];
		element1++;
		kont++;
	}
	while (element2 < rozmir) {
		konteiner[kont] = massiv[element2];
		element2++;
		kont++;
	}
	memcpy(massiv, konteiner, rozmir * sizeof (int));
}