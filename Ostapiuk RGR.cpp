#include <iostream>
#include <iomanip>
#include <chrono>
using namespace std;
long long sortuv_vstavkami_iter(int * massiv, const int& size) {  
// Алгоритм сортування вставками (ітераційна)
	auto pochatok = std::chrono::high_resolution_clock::now();
	int klyuch, j;
	for (int i = 1; i < size; ++i) {
		klyuch = massiv[i];
		j = i - 1;
		while (j >= 0 && massiv[j] > klyuch) {
			massiv[j + 1] = massiv[j];
			j = j - 1;
		}
		massiv[j + 1] = klyuch;
	}
	auto kinets = chrono::high_resolution_clock::now();
	auto truvalist = chrono::duration_cast<chrono::milliseconds>(kinets - pochatok).count();
	return truvalist;
}
long long sortuv_vstavkami_paral(int * massiv, const int& size) { 
// Алгоритм сортування вставками (паралельна)
	auto pochatok = std::chrono::high_resolution_clock::now();
	int klyuch, j;
#pragma omp parallel for shared(massiv) private(klyuch, j)
	for (int i = 1; i < size; ++i) {
		klyuch = massiv[i];
		j = i - 1;
		while (j >= 0 && massiv[j] > klyuch) {
			massiv[j + 1] = massiv[j];
			j = j - 1;
		}
		massiv[j + 1] = klyuch;
	}
	auto kinets = chrono::high_resolution_clock::now();
	auto truvalist = chrono::duration_cast<chrono::milliseconds>(kinets - pochatok).count();
	return truvalist;
}
struct Compare { int znach; int indeks; };
#pragma omp declare reduction(maximum : struct Compare : omp_out = omp_in.val > omp_out.val ? omp_in : omp_out)
long long sortuv_viborom_paral(int* massiv, int size) {  
// Алгоритм сортування вибором (паралельна версія)
	auto pochatok = std::chrono::high_resolution_clock::now();
	for (int i = size - 1; i > 0; --i) {
		struct Compare max;
		max.znach = massiv[i];
		max.indeks = i;
		#pragma omp parallel for reduction(max:max)
		for (int j = i - 1; j >= 0; --j) {
			if (massiv[j] > max.znach) {
				max.znach = massiv[j];
				max.indeks = j;
			}
		}
		int konteiner = massiv[i];
		massiv[i] = max.znach;
		massiv[max.indeks] = konteiner;
	}
	auto kinets = chrono::high_resolution_clock::now();
	auto truvalist = chrono::duration_cast<chrono::milliseconds>(kinets - pochatok).count();
	return truvalist;
}
void zapovnennja_massuva(int * massiv1, int * massiv2, int * massiv3, const int& size) {
#pragma omp parallel for shared(massiv1)
	for (int i = 0; i < size; ++i) {
		massiv1[i] = rand() % 100;
		massiv2[i] = massiv1[i];
		massiv3[i] = massiv1[i];
	}
}
int main() {
	setlocale(LC_CTYPE, "ukr");
	srand((unsigned)time(0));
	int rozmiru[] = { 1000, 5000, 10000, 50000, 100000, 500000, 1000000 };
	for (int rozmir : rozmiru) {
		// generate three arrays, one for every sort method
		int * massiv1 = new int[rozmir];
		int * massiv2 = new int[rozmir];
		int * massiv3 = new int[rozmir];
		zapovnennja_massuva(massiv1, massiv2, massiv3, rozmir);
		// реалізація сортування
		auto chas_algor_vstavkami_iter = sortuv_vstavkami_iter(massiv1, rozmir);
		auto chas_algor_vstavkami_paral = sortuv_vstavkami_paral(massiv2, rozmir);
		auto chas_algor_viborom = sortuv_viborom_paral(massiv3, rozmir);
		// Виведення
		cout << "Розмір масиву складає: " << rozmir << ", а час ітераційного сортування вставками виконується за: " << chas_algor_vstavkami_iter << " мс" << endl;
		cout << "Розмір масиву складає: " << rozmir << ", а час паралельного сортування вставками виконується за: " << chas_algor_vstavkami_paral << " мс" << endl;
		cout << "Розмір масиву складає: " << rozmir << ", а час паралельного сортування вибором виконується за: " << chas_algor_viborom << " мс" << endl << endl;
		// Очистити пам'ять від масивів
		delete[] massiv1;
		delete[] massiv2;
		delete[] massiv3;
	}
	system("pause");
	return 0;