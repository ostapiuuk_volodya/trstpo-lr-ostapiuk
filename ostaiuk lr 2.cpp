#include<iostream>
#include <list>
#include <limits.h>
#include <chrono>
#include <math.h>
#include <vector>
using namespace std;
using Matrix = vector<vector<int>>;
int main(){
	setlocale(LC_CTYPE, "ukr");
	size_t rozmir = 20;
	size_t VisotaM = 150;
	size_t ShirinaM = 150; //забезпечення кількості елементів більше 25000
	srand(14159);
	Matrix massiv;
	massiv.reserve(VisotaM);
	// Заповненян масиву
	for (int i = 0; i < VisotaM; ++i) {
		vector<int> v;
		v.reserve(ShirinaM);

		for (int j = 0; j < ShirinaM; ++j) {
			int val = rand() % rozmir;
			v.push_back(val);
		}

		massiv.push_back(move(v));
	}
	// Обрахунок
	unsigned long long summa = 0;
	#pragma omp parallel for reduction(+:summa)  //задання оператора додавання та списку загальної змінної
	for (int i = 0; i < VisotaM; ++i) {
		for (int j = 1; j < ShirinaM; j += 2) { //проходження непарних стовпців
			auto znach = massiv[i][j];
			summa += znach;  //сумування елементів
		}
	}
	cout << "Результат обрахунку непарних стовпців складає: " << summa << endl;
	system("pause");
	return 0;
}