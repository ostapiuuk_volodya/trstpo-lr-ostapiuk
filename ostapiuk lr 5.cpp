#include<iostream>
#include <list>
#include <limits.h>
#include <chrono>
using namespace std;
class Graf {
	int V;    // номер вершини
	list<int> *sumizhn;    // Покажчик масиву, що містить списки суміжності
	bool vidvidannja_vershin(int v, bool vidnidani[], bool *rekurs_stek){ // використовуєтьсяу ф-ії ciklichna_perevirka()
		if (vidnidani[v] == false) {
			//Позначення вершину як відвіданої та частиною рекурсивний стеку
			vidnidani[v] = true;
			rekurs_stek[v] = true;
			// Повернутися до всіх вершин, що прилягають до цієї вершини
			list<int>::iterator i;
			for (i = sumizhn[v].begin(); i != sumizhn[v].end(); ++i)
			{
				if (!vidnidani[*i] && vidvidannja_vershin(*i, vidnidani, rekurs_stek))
					return true;
				else if (rekurs_stek[*i])
					return true;
			}
		}
		rekurs_stek[v] = false;  // видалити вершину з рекурсивний стеку
		return false;
	}
public:
	Graf(int V) {  // Конструктор
		this->V = V;
		sumizhn = new list<int>[V];
	}
	void dodatu_vershinu(int ver, int hor) {
		sumizhn[ver].push_back(hor);
	}
	bool ciklichna_perevirka() {   // повертає true, якщо в гарфі є цикл
		// Позначає всі вершини як не відвідані і не як частину рекурсії
		bool *vidnidani = new bool[V];
		bool *rekurs_stek = new bool[V];
		for (int i = 0; i < V; i++) {
			vidnidani[i] = false;
			rekurs_stek[i] = false;
		}
		// Виклик рекурсії, щоб виявити цикл у дереві пошуком вглиб
		bool ciklichna_perevirka = false;
#pragma omp parallel for
		for (int i = 0; i < V; i++) {
			if (vidvidannja_vershin(i, vidnidani, rekurs_stek))
				ciklichna_perevirka = true;
		}
		return ciklichna_perevirka;
	}
};
int main() {
	setlocale(LC_CTYPE, "ukr");
	int size = 8000;
	srand(14159);
	Graf derevo(size); //створення графа
	for (int i = 0; i < size; ++i) {
		int ver = rand() % 100;
		int hor = rand() % 100;
		derevo.dodatu_vershinu(ver, hor);
	}
	if (derevo.ciklichna_perevirka())
		cout << "Граф є циклічним, а тому не є деревом\n";
	else
		cout << "Граф є ациклічним, а тому є деревом!\n";
	system("pause");
	return 0;
}